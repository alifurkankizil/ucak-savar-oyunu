﻿namespace UcakSavar
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelUst = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelYardim = new System.Windows.Forms.Label();
            this.pictureUcaksavar = new System.Windows.Forms.PictureBox();
            this.timerAtes = new System.Windows.Forms.Timer(this.components);
            this.timerUcak = new System.Windows.Forms.Timer(this.components);
            this.timerUcakIleri = new System.Windows.Forms.Timer(this.components);
            this.timerSure = new System.Windows.Forms.Timer(this.components);
            this.panelUst.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureUcaksavar)).BeginInit();
            this.SuspendLayout();
            // 
            // panelUst
            // 
            this.panelUst.BackColor = System.Drawing.Color.LightSeaGreen;
            this.panelUst.Controls.Add(this.label4);
            this.panelUst.Controls.Add(this.label3);
            this.panelUst.Controls.Add(this.label2);
            this.panelUst.Controls.Add(this.label1);
            this.panelUst.Controls.Add(this.labelYardim);
            this.panelUst.Location = new System.Drawing.Point(-1, 0);
            this.panelUst.Name = "panelUst";
            this.panelUst.Size = new System.Drawing.Size(1035, 63);
            this.panelUst.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.ForeColor = System.Drawing.Color.Gold;
            this.label4.Location = new System.Drawing.Point(935, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "0";
            this.label4.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.ForeColor = System.Drawing.Color.Gold;
            this.label3.Location = new System.Drawing.Point(665, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "0";
            this.label3.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(880, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Vurulan Uçak Sayısı";
            this.label2.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(641, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Geçen Süre";
            this.label1.Visible = false;
            // 
            // labelYardim
            // 
            this.labelYardim.AutoSize = true;
            this.labelYardim.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelYardim.ForeColor = System.Drawing.SystemColors.Info;
            this.labelYardim.Location = new System.Drawing.Point(8, 7);
            this.labelYardim.Name = "labelYardim";
            this.labelYardim.Size = new System.Drawing.Size(422, 64);
            this.labelYardim.TabIndex = 0;
            this.labelYardim.Text = "Oyunu başlatmak için ENTER tuşuna basın.\r\nUçaksavarı hareket ettirmek için SAĞ/SO" +
    "L YÖN TUŞLARINI kullanın.\r\nAteş etmek için BOŞLUK tuşuna basın.\r\n\r\n";
            // 
            // pictureUcaksavar
            // 
            this.pictureUcaksavar.Location = new System.Drawing.Point(986, 568);
            this.pictureUcaksavar.Name = "pictureUcaksavar";
            this.pictureUcaksavar.Size = new System.Drawing.Size(48, 41);
            this.pictureUcaksavar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureUcaksavar.TabIndex = 1;
            this.pictureUcaksavar.TabStop = false;
            // 
            // timerAtes
            // 
            this.timerAtes.Interval = 200;
            this.timerAtes.Tick += new System.EventHandler(this.timerAtes_Tick);
            // 
            // timerUcak
            // 
            this.timerUcak.Interval = 1600;
            this.timerUcak.Tick += new System.EventHandler(this.timerUcak_Tick);
            // 
            // timerUcakIleri
            // 
            this.timerUcakIleri.Interval = 200;
            this.timerUcakIleri.Tick += new System.EventHandler(this.timerUcakIleri_Tick);
            // 
            // timerSure
            // 
            this.timerSure.Interval = 1000;
            this.timerSure.Tick += new System.EventHandler(this.timerSure_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(1034, 609);
            this.Controls.Add(this.pictureUcaksavar);
            this.Controls.Add(this.panelUst);
            this.Name = "Form1";
            this.Text = "UcakSavar";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.panelUst.ResumeLayout(false);
            this.panelUst.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureUcaksavar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelUst;
        private System.Windows.Forms.Label labelYardim;
        private System.Windows.Forms.PictureBox pictureUcaksavar;
        private System.Windows.Forms.Timer timerAtes;
        private System.Windows.Forms.Timer timerUcak;
        private System.Windows.Forms.Timer timerUcakIleri;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timerSure;
    }
}

