﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UcakSavar
{
    public partial class Form1 : Form
    {

        private bool oyunDurumu = false;
        private int mermiSayisi = 40;
        private int ates = 0;
        private int ucak = 0;
        private int sure = 0;
        private int vurulanUcak = 0;

        private string ucaksavarKonum = "C:\\ucaksavar.jpg";
        private string ucakKonum = "C:\\ucak.jpg";
        private string atesKonum = "C:\\ates.jpg";


        PictureBox[] atesler = new PictureBox[40];
        PictureBox[] ucaklar = new PictureBox[20];

        public void OyunBaslangic()
        {
            for (int i = 0; i < mermiSayisi; i++)
            {
                atesler[i] = new PictureBox();
                atesler[i].Image = Image.FromFile(atesKonum);
                atesler[i].Size = new Size(30, 30);
                atesler[i].Location = new Point(-10, -10);
            }
            for (int i = 0; i < 20; i++)
            {
                ucaklar[i] = new PictureBox();
                ucaklar[i].Image = Image.FromFile(ucakKonum);
                ucaklar[i].Size = new Size(30, 30);
                ucaklar[i].Location = new Point(-100, -100);
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void AtesOlustur(int x)
        {
            atesler[ates].Location = new Point(x, 510);
            this.Controls.Add(atesler[ates]);
            AtesArttir();
        }
        public void UcakOlustur()
        {
            Random rnd = new Random();

            ucaklar[ucak].Location = new Point(rnd.Next(10, 980), 12);
            this.Controls.Add(ucaklar[ucak]);
            UcakArttir();
        }
        public void UcakArttir()
        {
            if (ucak == 19)
            {
                ucak = 0;
            }
            else
            {
                ucak++;
            }
        }
        public void AtesArttir()
        {
            if (ates == mermiSayisi-1)
            {
                ates = 0;
            }
            else
            {
                ates++;
            }
        }
        private int xKoordinati()
        {
            return pictureUcaksavar.Location.X;
        }
        private int yKoordinati()
        {
            return pictureUcaksavar.Location.Y;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {

            int basilanTus = e.KeyValue;

            //int xKoordinati = pictureUcaksavar.Location.X;
            //int yKoordinati = pictureUcaksavar.Location.Y;

            

            int hiz = 40;

            switch (basilanTus)
            {
                //Enter
                case 13:
                    if (oyunDurumu == false)
                    {
                        timerUcak.Start();
                        timerUcakIleri.Start();
                        timerAtes.Start();
                        timerSure.Start();

                        oyunDurumu = true;

                        labelYardim.Visible = false;
                        label1.Visible = true;
                        label2.Visible = true;
                        label3.Visible = true;
                        label4.Visible = true;
                    }
                    break;
                //Boşluk
                case 32:
                    if (oyunDurumu == true)
                    {
                        AtesOlustur(xKoordinati());
                    }
                    
                    break;
                //Sol
                case 37:
                    if (xKoordinati() > 5)
                    {
                        pictureUcaksavar.Location = new Point(xKoordinati() - hiz, yKoordinati());
                    }                    
                    break;
                //Sağ
                case 39:
                    if (xKoordinati() < 996)
                    {
                        pictureUcaksavar.Location = new Point(xKoordinati() + hiz, yKoordinati());
                    }
                    break;
                //Default
                default:
                    break;
            }
        }

        private void timerAtes_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < mermiSayisi; i++)
            {
                if(atesler[i].Location.X!=-10 && atesler[i].Location.Y != -10)
                {
                    int x = atesler[i].Location.X;
                    int y = atesler[i].Location.Y;

                    atesler[i].Location = new Point(x, y - 40);
                }
                
            }
        }

        private void timerUcak_Tick(object sender, EventArgs e)
        {
            UcakOlustur();
        }

        private void timerUcakIleri_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < 20; i++)
            {
                int x = ucaklar[i].Location.X;
                int y = ucaklar[i].Location.Y;

                if (x !=-100 && y != -100)
                {
                    if (y > 570)
                    {
                        OyunSonu();
                    }
                    else
                    {
                        ucaklar[i].Location = new Point(x, y + 20);
                    }
                }
                
            }
            UcakCarpismaKontrol();
        }
        private void UcakCarpismaKontrol()
        {
            int sinir = 30;
            for (int i = 0; i < 40; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    if(ucaklar[j].Location.X+sinir>atesler[i].Location.X && ucaklar[j].Location.X - sinir < atesler[i].Location.X)
                    {
                        if(ucaklar[j].Location.Y-sinir<atesler[i].Location.Y && ucaklar[j].Location.Y + sinir > atesler[i].Location.Y)
                        {
                            ucaklar[j].Location = new Point(-100, -100);
                            atesler[i].Location = new Point(-10, -10);
                            vurulanUcak++;
                            label4.Text = vurulanUcak.ToString();
                        }
                        
                    }
                }
            }
        }
        private void OyunSonu()
        {
            timerUcak.Stop();
            timerUcakIleri.Stop();
            timerAtes.Stop();
            timerSure.Stop();
           

            labelYardim.Visible = true;
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;

            EkranTemizle();

            oyunDurumu = false;

            MessageBox.Show("Tebrikler " + sure + " Saniyede " + vurulanUcak + " Adet Uçak Vurdunuz ");
            vurulanUcak = 0;
            sure = 0;

            label4.Text = "0";
            label3.Text = "0";
        }
        public void EkranTemizle()
        {
            for (int i = 0; i < mermiSayisi; i++)
            {
                atesler[i].Location = new Point(-10, -10);
            }
            for (int i = 0; i < 20; i++)
            {
                ucaklar[i].Location = new Point(-100, -100);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureUcaksavar.Image = Image.FromFile(ucaksavarKonum);
            OyunBaslangic();
        }

        private void timerSure_Tick(object sender, EventArgs e)
        {
            sure++;
            label3.Text = sure.ToString();
            
        }
    }
}
